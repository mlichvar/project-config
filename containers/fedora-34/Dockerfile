# Copyright (C) 2021 Red Hat
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# This file is maintained with the ../containers.dhall configuration
FROM registry.fedoraproject.org/fedora:34
ENV HOME=/workspace
ENV XDG_CACHE_HOME=/workspace/.cache
ENV PATH=/workspace/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# OpenShift container might run as any user, so we need to make sure the workspace is writable:
# https://docs.openshift.com/container-platform/4.2/openshift_images/create-images.html#images-create-guide-openshift_create-images
WORKDIR /workspace
RUN mkdir -p /workspace/.local && chgrp -R 0 /workspace && chmod -R g=u /workspace
RUN dnf update -y && dnf install -y iproute rsync git traceroute unzip bzip2 make curl wget tar procps-ng which sudo unzip findutils grep ncurses-devel openssl-devel zlib-devel krb5-devel make cmake gcc gcc-c++ rpm-build rpm-sign expect python3-setuptools python2-setuptools python3 python3-devel python3-wheel python3-pip && dnf clean all
RUN ln -sf /bin/pip3 /bin/pip && /bin/pip3 install --user 'tox>=3.8.0'
# This run create a sudo configuration so that when a container running as root delete /etc/sudoers.d/zuul,
# then it can no longer run sudo, satisfying the revoke-sudo zuul jobs role.
RUN echo 'zuul:x:0:0:root:/workspace:/bin/bash' >> /etc/passwd && mv /etc/sudoers /etc/sudoers.d/zuul && grep includedir /etc/sudoers.d/zuul > /etc/sudoers && sed -e 's/.*includedir.*//' -i /etc/sudoers.d/zuul && chmod 440 /etc/sudoers
# Workaround for k1s
RUN ln -s /workspace/src /root/src && ln -s /workspace/.local /root/.local
